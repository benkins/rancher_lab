# Rancher Labo

DEPRECATION NOTE: This github repo is for example deployments only.

This repo includes the docker-compose orchestration for deployment of a Rancher server in a single node setup with an external mysql and a nginx reverse proxy.

### How to install it
You must have Docker and Docker compose installed on your host.
```
git clone https://github.com/beninanutshell/rancher_lab.git
cd rancher_lab
```

### Configure the secrets
```
vi dbsecrets.env
```

### Configure your nginx.conf
```
vi ./nginx/conf/nginx.conf
```

### Run the full stack:
```
docker-compose up -d
```

Go to https://nginx_ip/ to view the Rancher server UI.
See the rancher official upgrade documentation.

Troubleshooting
See more on official page
